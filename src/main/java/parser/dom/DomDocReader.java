package parser.dom;

import model.entity.Flower;
import model.entity.GrowingTip;
import model.entity.VisualParameter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DomDocReader {
    public List<Flower> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Flower> flowers = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("flower");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Flower flower = new Flower();
            GrowingTip growingTip;
            VisualParameter visualParameters;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                flower.setName(element.getElementsByTagName("name").item(0).getTextContent());
                flower.setOriginCountry(element.getElementsByTagName("originCountry").item(0).getTextContent());
                growingTip = getGrowingTip(element.getElementsByTagName("growingTip"));
                visualParameters = getVisualParam(element.getElementsByTagName("visualParameters"));
                flower.setGrowingTip(growingTip);
                flower.setVisualParameters(visualParameters);
                flowers.add(flower);
            }
        }
        return flowers;
    }

    private GrowingTip getGrowingTip(NodeList nodes){
        GrowingTip growingTip = new GrowingTip();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            growingTip.setTemperature(Integer.parseInt(element.getElementsByTagName("temperature").item(0).getTextContent()));
            growingTip.setLightLoving(Boolean.parseBoolean(element.getElementsByTagName("lightLoving").item(0).getTextContent()));
            growingTip.setWeekWatering(Integer.parseInt(element.getElementsByTagName("weekWatering").item(0).getTextContent()));
        }

        return growingTip;
    }
    private VisualParameter getVisualParam(NodeList nodes){
        VisualParameter visualParameter = new VisualParameter();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            visualParameter.setStemColor(element.getElementsByTagName("stemColor").item(0).getTextContent());
            visualParameter.setBloomColor(element.getElementsByTagName("bloomColor").item(0).getTextContent());
            visualParameter.setAverageSize(Integer.parseInt(element.getElementsByTagName("averageSize").item(0).getTextContent()));
        }
        return visualParameter;
    }
}


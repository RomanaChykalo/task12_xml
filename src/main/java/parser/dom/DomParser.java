package parser.dom;

import model.entity.Flower;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

public class DomParser {
    public static List<Flower> getFlowerList(File xml, File xsd) {
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();
        DomDocReader reader = new DomDocReader();
        return reader.readDoc(doc);
    }
}
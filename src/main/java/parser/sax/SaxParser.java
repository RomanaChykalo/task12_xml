package parser.sax;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import model.entity.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static model.consts.ExceptionConst.PARSCONFIGEXC;

public class SaxParser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    private static Logger logger = LogManager.getLogger(SaxParser.class);
    public static List<Flower> parseFlowers(File xml, File xsd) {
        List<Flower> flowerList = new ArrayList<>();
        try {
            saxParserFactory.setSchema(SaxValidator.createSchema(xsd));
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxHandler saxHandler = new SaxHandler();
            saxParser.parse(xml, saxHandler);
            flowerList = saxHandler.getFlowerList();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            logger.error(PARSCONFIGEXC);
        }
        return flowerList;
    }
}

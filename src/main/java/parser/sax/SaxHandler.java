package parser.sax;

import model.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {

    private List<Flower> flowerList = null;
    private Flower flower = null;
    private EarthType typeOfSoil = null;
    private GrowingTip growingTip = null;
    private Multiplying reproductionType = null;
    private VisualParameter parameter = null;

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    private boolean fName = false;
    private boolean fOriginCountry = false;

    private boolean fSoilType = false;
    private boolean fClay = false;
    private boolean fSoil = false;
    private boolean fTurf = false;

    private boolean fGrowingTip = false;
    private boolean fTemperature = false;
    private boolean fLightLoving = false;
    private boolean fWeekWatering = false;

    private boolean fvisualParameters = false;
    private boolean fAverageSize = false;
    private boolean fStemColor = false;
    private boolean fBloomColor = false;

    private boolean fReproductionType = false;
    private boolean fLeave = false;
    private boolean fCut = false;
    private boolean fSeed = false;


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("flower")) {
            String flowerId = attributes.getValue("flowerId");
            flower = new Flower();
            flower.setFlowerId(Integer.parseInt(flowerId));
            if (flowerList == null)
                flowerList = new ArrayList<>();
        } else if (qName.equalsIgnoreCase("name")) {
            fName = true;
        } else if (qName.equalsIgnoreCase("originCountry")) {
            fOriginCountry = true;
        } else if (qName.equalsIgnoreCase("typeOfSoil")) {
            fSoilType = true;
        } else if (qName.equalsIgnoreCase("CLAY")) {
            fClay = true;
        } else if (qName.equalsIgnoreCase("SOIL")) {
            fSoil = true;
        } else if (qName.equalsIgnoreCase("TURF")) {
            fTurf = true;
        } else if (qName.equalsIgnoreCase("growingTip")) {
            fGrowingTip = true;
        } else if (qName.equalsIgnoreCase("temperature")) {
            fTemperature = true;
        } else if (qName.equalsIgnoreCase("lightLoving")) {
            fLightLoving = true;
        } else if (qName.equalsIgnoreCase("weekWatering")) {
            fWeekWatering = true;
        } else if (qName.equalsIgnoreCase("visualParameters")) {
            fGrowingTip = true;
        } else if (qName.equalsIgnoreCase("averageSize")) {
            fAverageSize = true;
        } else if (qName.equalsIgnoreCase("stemColor")) {
            fStemColor = true;
        } else if (qName.equalsIgnoreCase("bloomColor")) {
            fBloomColor = true;
        } else if (qName.equalsIgnoreCase("reproductionType")) {
            fReproductionType = true;
        } else if (qName.equalsIgnoreCase("LEAVES")) {
            fLeave = true;
        } else if (qName.equalsIgnoreCase("CUTTINGS")) {
            fCut = true;
        } else if (qName.equalsIgnoreCase("SEEDS")) {
            fSeed = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("flower")) {
            flowerList.add(flower);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (fName) {
            flower.setName(new String(ch, start, length));
            fName = false;
        } else if (fOriginCountry) {
            flower.setOriginCountry(new String(ch, start, length));
            fOriginCountry = false;
        } else if (fClay) {
            flower.setTypeOfSoil(EarthType.CLAY);
            fClay = false;
        } else if (fSoil) {
            flower.setTypeOfSoil(EarthType.SOIL);
            fSoil = false;
        } else if (fTurf) {
            flower.setTypeOfSoil(EarthType.TURF);
            fTurf = false;
        } else if (fSoilType) {
            flower.setTypeOfSoil(EarthType.SOIL);
            fOriginCountry = false;
        } else if (fGrowingTip) {
            growingTip = new GrowingTip();
            fGrowingTip = false;
        } else if (fTemperature) {
            int temp = Integer.parseInt(new String(ch, start, length));
            growingTip.setTemperature(temp);
            fTemperature = false;
        } else if (fLightLoving) {
            boolean lightlove = Boolean.parseBoolean(new String(ch, start, length));
            growingTip.setLightLoving(lightlove);
            fLightLoving = false;
        } else if (fWeekWatering) {
            int weekWater = Integer.parseInt(new String(ch, start, length));
            growingTip.setWeekWatering(weekWater);
            fWeekWatering = false;
        } else if (fvisualParameters) {
            parameter = new VisualParameter();
            fvisualParameters = false;
        } else if (fAverageSize) {
            int size = Integer.parseInt(new String(ch, start, length));
            parameter.setAverageSize(size);
            fAverageSize = false;
        } else if (fBloomColor) {
            parameter.setBloomColor(new String(ch, start, length));
            fBloomColor = false;
        } else if (fStemColor) {
            parameter.setStemColor(new String(ch, start, length));
            fStemColor = false;
            flower.setVisualParameters(parameter);
            fWeekWatering = false;
        } else if (fLeave) {
            flower.setReproductionType(Multiplying.LEAVES);
            fLeave = false;
        } else if (fSeed) {
            flower.setReproductionType(Multiplying.SEEDS);
            fSeed = false;
        } else if (fCut) {
            flower.setReproductionType(Multiplying.CUTTINGS);
            fCut = false;
        }
    }
}

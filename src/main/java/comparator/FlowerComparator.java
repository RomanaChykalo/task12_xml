package comparator;

import model.entity.Flower;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {
    public int compare(Flower o1, Flower o2) {
        if(o1.getName().length()>o2.getName().length()){
            return 1;
        }
        if(o1.getName().length()<o2.getName().length()){
            return -1;
        }
        else {
            return 0;
        }
    }
}

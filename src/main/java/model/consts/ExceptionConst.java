package model.consts;

public class ExceptionConst {
    public static final String PARSCONFIGEXC = "parsing error";
    public static final String IOEXC = "problem with file reading";
    public static final String GENERALEXC = "general exception was happend";
}

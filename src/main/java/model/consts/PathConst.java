package model.consts;

public class PathConst {
    public static final String XMLPATH = "src\\main\\resources\\xml\\flowerXML.xml";
    public static final String XSDPATH = "src\\main\\resources\\xml\\flowerXSD.xsd";
}

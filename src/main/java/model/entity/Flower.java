package model.entity;

public class Flower {
    private int flowerId;
    private String name;
    private String originCountry;
    private EarthType typeOfSoil;
    private GrowingTip growingTip;
    private VisualParameter visualParameters;
    private Multiplying reproductionType;

    public Flower(){}

    public Flower(String name, EarthType typeOfSoil, String originCountry, GrowingTip growingTip,
                  VisualParameter visualParameters, Multiplying reproductionType) {
        this.name = name;
        this.typeOfSoil = typeOfSoil;
        this.originCountry = originCountry;
        this.growingTip = growingTip;
        this.visualParameters = visualParameters;
        this.reproductionType = reproductionType;
    }
    public void setFlowerId(int flowerId) {
        this.flowerId = flowerId;
    }
    public int getFlowerId() {
        return flowerId;
    }
    public String getName() {
        return name;
    }

    public EarthType getTypeOfSoil() {
        return typeOfSoil;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public GrowingTip getTip() {
        return growingTip;
    }

    public VisualParameter getVisualParameters() {
        return visualParameters;
    }

    public Multiplying getReproductionType() {
        return reproductionType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public void setTypeOfSoil(EarthType typeOfSoil) {
        this.typeOfSoil = typeOfSoil;
    }

    public void setGrowingTip(GrowingTip growingTip) {
        this.growingTip = growingTip;
    }

    public void setVisualParameters(VisualParameter visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setReproductionType(Multiplying reproductionType) {
        this.reproductionType = reproductionType;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", originCountry='" + originCountry + '\'' +
                ", typeOfSoil=" + typeOfSoil +
                ", growingTip=" + growingTip +
                ", visualParameterList=" + visualParameters +
                ", reproductionType=" + reproductionType +
                '}';
    }
}

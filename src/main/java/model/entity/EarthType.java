package model.entity;

public enum EarthType {
    CLAY, SOIL, TURF
}

package model.entity;

public class VisualParameter {
    private int averageSize;
    private String stemColor;
    private String bloomColor;

    public VisualParameter(int averageSize, String stemColor, String bloomColor) {
        this.averageSize = averageSize;
        this.stemColor = stemColor;
        this.bloomColor = bloomColor;
    }
    public VisualParameter(){}
    public int getAverageSize() {
        return averageSize;
    }

    public String getStemColor() {
        return stemColor;
    }

    public String getBloomColor() {
        return bloomColor;
    }

    public void setAverageSize(int averageSize) {
        this.averageSize = averageSize;
    }

    public void setStemColor(String stemColor) {
        this.stemColor = stemColor;
    }

    public void setBloomColor(String bloomColor) {
        this.bloomColor = bloomColor;
    }

    @Override
    public String toString() {
        return "VisualParameter{" +
                "averageSize=" + averageSize +
                ", stemColor='" + stemColor + '\'' +
                ", bloomColor='" + bloomColor + '\'' +
                '}';
    }
}

package model.entity;

public class GrowingTip {
    private int temperature;
    private boolean lightLoving;
    private int weekWatering;

    public GrowingTip(int temperature, boolean lightLoving, int weekWatering) {
        this.temperature = temperature;
        this.lightLoving = lightLoving;
        this.weekWatering = weekWatering;
    }

    public GrowingTip() {
    }

    public int getTemperature() {
        return temperature;
    }

    public boolean isLightLoving() {
        return lightLoving;
    }

    public int getWeekWatering() {
        return weekWatering;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setLightLoving(boolean lightLoving) {
        this.lightLoving = lightLoving;
    }

    public void setWeekWatering(int weekWatering) {
        this.weekWatering = weekWatering;
    }

    @Override
    public String toString() {
        return "GrowingTip{" +
                "temperature=" + temperature +
                ", lightLoving=" + lightLoving +
                ", weekWatering=" + weekWatering +
                '}';
    }
}

import comparator.FlowerComparator;
import model.entity.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import parser.dom.DomParser;
import parser.sax.SaxParser;

import java.io.File;
import java.util.*;

import static model.consts.PathConst.XMLPATH;
import static model.consts.PathConst.XSDPATH;

public class App {
    private static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        File xml = new File(XMLPATH);
        File xsd = new File(XSDPATH);
        printList(SaxParser.parseFlowers(xml, xsd), "SAX");
        printList(DomParser.getFlowerList(xml, xsd), "DOM");
    }

    private static void printList(List<Flower> flowers, String parserName) {
        Collections.sort(flowers, new FlowerComparator());
        logger.info(parserName);
        for (Flower flower : flowers) {
            logger.info(flower);
        }
    }
}
